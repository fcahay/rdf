# RDF

## Lancement

### Zookeeper

[Téléchargement](https://www.apache.org/dyn/closer.cgi/zookeeper)

```shell
# In /bin directory
./zkServer.sh start
```

### Kafka

[Téléchargement](https://kafka.apache.org/downloads)

Server :
```shell
# In /bin directory
./kafka-server-start.sh -daemon ../config/server.properties
```
Topic :
```shell
./kafka-topics.sh --create --bootstrap-server localhost:9092 --replication-factor 1 --partitions 1 --topic <topic_name>
```
Producer :
```shell
# In /bin directory
./kafka-console-producer.sh --bootstrap-server localhost:9092 --topic <topic_name>
```
Consumer :
```shell
# In /bin directory
./kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic <topic_name> --from-beginning
```

## Etat du projet

On a fait jusqu'à la question 3, mais on n'a pas réussi à faire la suite.
On produit des données que l'on envoie vers le topic `vaccinated` via AVRO.


## Difficultés rencontrées

- Peu d'exemple d'utilisation de KafkaStream sur internet
- L'utilisation de Kafka et Zookeeper en ligne de commande n'est pas intuitif (suppression de topic)
- Impossible de faire fonctionner le producer et le consumer avec Java

## Contributions des membres du groupe
(Moyenne des notes de tous les membres)
- Florian : 2.75
- Kévin   : 2.25
- Kosal   : 1.75
- Loris    : 2.25

## Conclusion

