import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.github.javafaker.Faker;
import com.twitter.bijection.Bijection;
import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodec;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.reflect.ReflectData;

import java.util.Random;

@JsonDeserialize
public class Person implements IPerson {
    private static final Faker faker = new Faker();
    private static final VACCIN[] vaccinValues = VACCIN.values();
    private static final SIDEEFFECT[] sideEffectValues = SIDEEFFECT.values();
    private static final Schema schema = ReflectData.get().getSchema(Person.class);
    private static final Injection<GenericRecord, byte[]> binRecord = GenericAvroCodecs.toBinary(schema);

    private String id;
    private String lastName;
    private String firstName;
    private String birthDay;
    private String zipcode;
    private String genre;
    private String vaccin;

    private String sideEffect;
    private String siderCode;

    public Person() {
    }

    public Person(long id, boolean isStudent) {
        this.id = id + "";
        this.lastName = faker.name().lastName();
        this.firstName = faker.name().firstName();
        if (isStudent) {
            this.birthDay = faker.date().birthday(20, 30).toString();
        } else {
            this.birthDay = faker.date().birthday(30, 70).toString();
        }
        this.zipcode = faker.address().zipCode();
        this.genre = faker.number().numberBetween(1, 3) == 1 ? "MAN" : "WOMAN";
        this.vaccin = vaccinValues[new Random().nextInt(vaccinValues.length)].toString();

        if (vaccin.equals(VACCIN.NoVaccinated.toString())) {
            this.sideEffect = "";
            this.siderCode = "";
        } else {
            var effect = sideEffectValues[new Random().nextInt(sideEffectValues.length)];
            this.sideEffect = effect.name();
            this.siderCode = effect.getSider();
        }
    }

    public Schema getSchema() {
        return schema;
    }

    @Override
    public IPerson anonymize() {
        firstName = "";
        lastName = "";
        return this;
    }

    public byte[] toAvro() {
        var avroRecord = new GenericData.Record(schema);
        var binRecord = GenericAvroCodecs.toBinary(schema);

        avroRecord.put("birthDay", birthDay);
        avroRecord.put("firstName", firstName);
        avroRecord.put("genre", genre);
        avroRecord.put("id", id);
        avroRecord.put("lastName", lastName);
        avroRecord.put("sideEffect", sideEffect);
        avroRecord.put("siderCode", siderCode);
        avroRecord.put("vaccin", vaccin);
        avroRecord.put("zipcode", zipcode);
        return binRecord.apply(avroRecord);
    }

    public static Person fromAvro(byte[] bytes) {
        var genericRecord = binRecord.invert(bytes).get();
        var p = new Person();
        p.setFirstName(genericRecord.get("firstName").toString());
        p.setLastName(genericRecord.get("lastName").toString());
        p.setBirthDay(genericRecord.get("birthDay").toString());
        p.setGenre(genericRecord.get("genre").toString());
        p.setId(genericRecord.get("id").toString());
        p.setSideEffect(genericRecord.get("sideEffect").toString());
        p.setSiderCode(genericRecord.get("siderCode").toString());
        p.setVaccin(genericRecord.get("vaccin").toString());
        p.setZipcode(genericRecord.get("zipcode").toString());
        return p;
    }

    public boolean isVaccinated() {
        return !this.vaccin.equals(VACCIN.NoVaccinated.toString());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(String birthDay) {
        this.birthDay = birthDay;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getVaccin() {
        return vaccin;
    }

    public void setVaccin(String vaccin) {
        this.vaccin = vaccin;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public String getSiderCode() {
        return siderCode;
    }

    public void setSiderCode(String siderCode) {
        this.siderCode = siderCode;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", birthDay='" + birthDay + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", genre='" + genre + '\'' +
                ", vaccin='" + vaccin + '\'' +
                ", sideEffect='" + sideEffect + '\'' +
                ", siderCode='" + siderCode + '\'' +
                '}';
    }
}
