import org.apache.avro.Schema;

public interface IPerson {

    byte[] toAvro();

    IPerson anonymize();
}
