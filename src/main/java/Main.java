import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Statement;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicLong;

public class Main {
    private static final Model model = ModelFactory.createDefaultModel();
    private static final Model rdf = model.read("src/main/resources/lubm1.ttl");
    private static final OntModel inf = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM_MINI_RULE_INF);

    public static String VACCINATED_TOPIC = "vaccinated";
    public static String ANONYMIZE_TOPIC = "anonymousSideEffect";


    public static void main(String[] args) throws InterruptedException {
         inf.read("univ-bench.owl");

        var producer = AvroKafkaProducer.createProducer();

        var typeProperty = "http://www.w3.org/1999/02/22-rdf-syntax-ns#type";
        var rdfType = rdf.createProperty(typeProperty);

        var lastNameProperty = rdf.createProperty("www.uge.fr/property#lname");
        var firstNameProperty = rdf.createProperty("www.uge.fr/property#fname");
        var birthProperty = rdf.createProperty("www.uge.fr/property#birth");
        var zipCodeProperty = rdf.createProperty("www.uge.fr/property#zipcode");
        var genreProperty = rdf.createProperty("www.uge.fr/property#genre");
        var idProperty = rdf.createProperty("www.uge.fr/property#id");
        var vaccinNameProperty = rdf.createProperty("www.uge.fr/property#vaccinName");

        var counter = new AtomicLong();
        var person1 = inf.getOntClass("http://swat.cse.lehigh.edu/onto/univ-bench.owl#Person");
        person1.listSubClasses().forEach(subClass -> {
            var resource = rdf.createResource(subClass.toString());
            var values = rdf.listSubjectsWithProperty(rdfType, resource);

            var isStudent = subClass.toString().contains("student");

            while (values.hasNext()) {
                var person = new Person(counter.getAndIncrement(), isStudent);
                var element = values.nextResource();

                rdf.add(rdf.createStatement(element, lastNameProperty, person.getLastName()));
                rdf.add(rdf.createStatement(element, firstNameProperty, person.getFirstName()));
                rdf.add(rdf.createStatement(element, birthProperty, person.getBirthDay()));
                rdf.add(rdf.createStatement(element, zipCodeProperty, person.getZipcode()));
                rdf.add(rdf.createStatement(element, genreProperty, person.getGenre()));
                rdf.add(rdf.createStatement(element, idProperty, person.getId()));
                rdf.add(rdf.createStatement(element, vaccinNameProperty, person.getVaccin()));

                if (person.isVaccinated()) {
                    var record = new ProducerRecord<>(VACCINATED_TOPIC, person.getId(), person.toAvro());
                    producer.send(record);
                }
            }
        });

//        printSubjectsByProperty(vaccinNameProperty);
        producer.close();
        System.out.println("Producer fini");
        KafkaStream.runAnonymize();
    }

    public static void printSubjectsByProperty(Property property) {
        var subjects = rdf.listResourcesWithProperty(property);

        var list = new ArrayList<Statement>();
        while (subjects.hasNext()) {
            var element = subjects.nextResource();
            list.add(element.getProperty(property));
        }

        list.stream().distinct().forEach(System.out::println);
    }
}
