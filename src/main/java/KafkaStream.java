import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;

import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class KafkaStream {

    public static void runAnonymize() throws InterruptedException {
        var props = new Properties();
        props.put(StreamsConfig.APPLICATION_ID_CONFIG, "anonymize");
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.ByteArray().getClass().getName());

        var streamingConfig = new StreamsConfig(props);
        var builder = new StreamsBuilder();
        var latch = new CountDownLatch(1);


        var topicStream = builder.stream(Main.VACCINATED_TOPIC, Consumed.with(Serdes.String(), Serdes.ByteArray()));

        // update data
        var result = topicStream.mapValues((key, value) -> {
            var person = Person.fromAvro(value);
            person.anonymize();
            return person.toAvro();
        });

        // produce
        result.to(Main.ANONYMIZE_TOPIC);

        var kafkaStreams = new KafkaStreams(builder.build(), streamingConfig);

        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                kafkaStreams.close();
                latch.countDown();
            }
        });

        try {
            kafkaStreams.start();
            System.out.println("KafkaStream start");
            latch.await();
        } catch (Throwable e) {
            System.exit(1);
        }
    }


}
