public enum SIDEEFFECT {
    INJECTIONSITEPAIN("C0151828"),
    FATIGUE("C0015672"),
    HEADACHE("C0018681"),
    MUSCLEPAIN("C0231528"),
    CHILLS("C0085593"),
    JOINTPAIN("C0003862"),
    FEVER("C0015967"),
    INJECTIONSITESWELLING("C0151605"),
    INJECTIONSITEREDNESS("C0852625"),
    NAUSEA("C0027497"),
    MALAISE("C0231218"),
    LYMPHADENOPATHY("C0497156"),
    INJECTIONSITETENDERNESS("C0863083");

    private final String sider;

    SIDEEFFECT(String sider) {
        this.sider = sider;
    }

    public String getSider() {
        return sider;
    }
}
